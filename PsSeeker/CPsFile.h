/*
 * CPsFile.h
 *
 *  Created on: 2018年7月8日
 *      Author: zhengboyuan
 */

#ifndef CPSFILE_H_
#define CPSFILE_H_

#include "PsFile.h"
#include <string>
#include <vector>
#include "PsFileStream.h"
#include "TByteBuffer.h"


class CPsFile: public PsFile
{
public:
    CPsFile();
    virtual ~CPsFile();

    virtual void setFileIndex(int index);

    virtual int getFileIndex();


    virtual bool open(const char* filepath);

    virtual void close();

    virtual bool isOpen();

    virtual int64_t getTotalSize();

    virtual bool getTotalPositionRange(PsFilePosition& beginPos, PsFilePosition& endPos);


    virtual PsFilePosition getBeginPos();

    virtual PsFilePosition getEndPos();

    virtual PsFilePosition getBeginKeyPos();




    virtual bool getTotalKeyFrameRange(PsFilePosition& beginPos, PsFilePosition& endPos);

    virtual bool seek(time_t t, PsFileSeekMode seekMode, PsFilePosition& pos);

    virtual bool seekRange(time_t t, uint32_t duration, PsFileSeekMode seekMode, PsFilePosition& beginPos, PsFilePosition& endPos);


    virtual bool read(const PsFilePosition& pos, PsMPacket& pkt);

    virtual bool readRange(const PsFilePosition& beginPos, const PsFilePosition& endPos, PsPacketProc proc, void* userdata);


    virtual bool getMediaFormat(MFormat& fmt);

protected:
    typedef std::vector< PsFilePosition >   PsFilePositionArray;

protected:
    void scanFile();

    bool fetchBegin();

    bool convert(PsFileStream& stream, PsPacket& packet, PsFilePosition& position);

    bool hasScanned();

    size_t find(size_t offset, time_t t, PsFileSeekMode seekMode);

    size_t findGreater(size_t offset, time_t t, PsFileSeekMode seekMode);

    PsFilePosition getEndPos(PsFileSeekMode seekMode);
    size_t getEndPosIndex(PsFileSeekMode seekMode);

    size_t indexOf(const PsFilePosition& pos);

protected:
    int     m_index;
    std::string m_filepath;

    PsFilePositionArray m_positions;

    PsFileStream    m_stream;
    bool    m_scanned;

    comn::ByteBuffer    m_buffer;

};

#endif /* CPSFILE_H_ */
