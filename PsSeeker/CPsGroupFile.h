/*
 * CPsGroupFile.h
 *
 *  Created on: 2018年7月8日
 *      Author: zhengboyuan
 */

#ifndef CPSGROUPFILE_H_
#define CPSGROUPFILE_H_

#include "PsGroupFile.h"
#include <vector>
#include <string>
#include "SharedPtr.h"


class CPsGroupFile : public PsGroupFile
{
public:
    CPsGroupFile();
    virtual ~CPsGroupFile();


    virtual bool open(const char* filepath);

    virtual void close();

    virtual bool isOpen();

    virtual int64_t getTotalSize();


    virtual bool getTotalPositionRange(PsFilePosition& beginPos, PsFilePosition& endPos);


    virtual PsFilePosition getBeginPos();

    virtual PsFilePosition getEndPos();

    virtual PsFilePosition getBeginKeyPos();



    virtual bool getTotalKeyFrameRange(PsFilePosition& beginPos, PsFilePosition& endPos);

    virtual bool seek(time_t t, PsFileSeekMode seekMode, PsFilePosition& pos);

    virtual bool seekRange(time_t t, uint32_t duration, PsFileSeekMode seekMode, PsFilePosition& beginPos, PsFilePosition& endPos);


    virtual bool read(const PsFilePosition& pos, PsMPacket& pkt);

    virtual bool readRange(const PsFilePosition& beginPos, const PsFilePosition& endPos, PsPacketProc proc, void* userdata);



    virtual bool getMediaFormat(MFormat& fmt);


    virtual bool openEx(const FILE_LIST& files);

    virtual int getFileCount();

    virtual const char* getFilePath(int index);


protected:
    typedef std::shared_ptr<PsFile>     PsFilePtr;
    typedef std::vector< std::string >  StringArray;

    struct FileEntry
    {
        int index;
        std::string filepath;
        PsFilePtr   file;
    };

    typedef std::vector< FileEntry >    FileEntryArray;

protected:

    void closeAllEntry();

    size_t findEntry(size_t offset, time_t t, PsFileSeekMode seekMode);

    bool openEx(const StringArray& filepaths);

    void onOpen();

protected:
    FileEntryArray  m_entryArray;


};

#endif /* CPSGROUPFILE_H_ */
