/*
 * PsSeeker.h
 *
 *  Created on: 2018年7月8日
 *      Author: zhengboyuan
 */

#ifndef PSSEEKER_H_
#define PSSEEKER_H_


////////////////////////////////////////////////////////////////////////////

#ifdef WIN32

    #ifndef NOMINMAX
    #define NOMINMAX
    #endif //NOMINMAX

    #include <Windows.h>
#else

#endif //WIN32


#include <time.h>
////////////////////////////////////////////////////////////////////////////

#ifdef _MSC_VER
    typedef signed char     int8_t;
    typedef unsigned char   uint8_t;
    typedef short           int16_t;
    typedef unsigned short  uint16_t;
    typedef int             int32_t;
    typedef unsigned        uint32_t;
    typedef long long       int64_t;
    typedef unsigned long long   uint64_t;
#else
    #include <stdint.h>
    typedef void*   HANDLE;
#endif //_MSC_VER


///////////////////////////////////////////////////////////////////
#ifdef WIN32
    #ifndef DLLEXPORT
    #define DLLEXPORT __declspec(dllexport)
    #endif //DLLEXPORT
#else
    #define DLLEXPORT __attribute__ ((visibility ("default")))
#endif //WIN32

///////////////////////////////////////////////////////////////////
#ifdef __cplusplus
extern "C"
{
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct _FileList
{
    int count;    // 文件数量
    char filePath[10][256];    // 文件路径数组
}  FILE_LIST;


/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif



#endif /* PSSEEKER_H_ */
