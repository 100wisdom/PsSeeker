/*
 * DoubleByteBuffer.h
 *
 *  Created on: 2015年12月19日
 *      Author: terry
 */

#ifndef DOUBLEBYTEBUFFER_H_
#define DOUBLEBYTEBUFFER_H_

#include "ByteBuffer.h"


/**
 * 双缓冲, 即前后2个缓冲区, 可以切换使用
 *
 */
class DoubleByteBuffer
{
public:
	DoubleByteBuffer():
		m_buffer()
	{
		m_buffer = &m_front;
	}

	~DoubleByteBuffer()
	{
	}

	DoubleByteBuffer(const DoubleByteBuffer& obj):
		m_front(obj.m_front),
		m_back(obj.m_back),
		m_buffer()
	{
		m_buffer = &m_front;
	}

	DoubleByteBuffer& operator = (const DoubleByteBuffer& obj)
	{
		m_front = obj.m_front;
		m_back = obj.m_back;

		if (obj.isFront())
		{
			m_buffer = &m_front;
		}
		else
		{
			m_buffer = &m_back;
		}

		return *this;
	}

	/**
	 * 获取当前缓冲区
	 * @return
	 */
	ByteBuffer& getCurBuffer()
	{
		return *m_buffer;
	}

	/**
	 * 切换缓冲区
	 */
	ByteBuffer& switchBuffer()
	{
		if (m_buffer != &m_front)
		{
			m_buffer = &m_front;
		}
		else
		{
			m_buffer = &m_back;
		}
		return *m_buffer;
	}

	/**
	 * 当前缓存是否是前缓冲区
	 * @return
	 */
	bool isFront() const
	{
		return (m_buffer == &m_front);
	}

	/**
	 * 清理
	 */
	void clear()
	{
		m_front.clear();
		m_back.clear();
	}

	void ensure(size_t capacity)
	{
		m_front.ensure(capacity);
		m_back.ensure(capacity);
	}
protected:
	ByteBuffer	m_front;
	ByteBuffer	m_back;
	ByteBuffer*	m_buffer;


};


#endif /* DOUBLEBYTEBUFFER_H_ */
