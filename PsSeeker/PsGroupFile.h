/*
 * PsGroupFile.h
 *
 *  Created on: 2018年7月8日
 *      Author: zhengboyuan
 */

#ifndef PSGROUPFILE_H_
#define PSGROUPFILE_H_

#include <stdint.h>
#include <time.h>
#include <stdio.h>

#include "MFormat.h"
#include "PsFile.h"
#include "PsSeeker.h"


/**
 * 将多个PS文件虚拟为单个PS文件
 */
class PsGroupFile : public PsFile
{
public:


public:
    virtual ~PsGroupFile() {}

    /**
     * 打开文件
     * @param filepaths 文件路径数组
     * @param count     路径数组长度
     * @return
     */
    virtual bool openEx(const FILE_LIST& files) =0;

    /**
     * 获取文件数量
     * @return
     */
    virtual int getFileCount() =0;

    virtual const char* getFilePath(int index) =0;

};

#endif /* PSGROUPFILE_H_ */
