/*
 * PsCutTime.h
 *
 *  Created on: 2018年7月13日
 *      Author: zhengboyuan
 */

#ifndef PSCUTTIME_H_
#define PSCUTTIME_H_

#include "PsFileStream.h"
#include "TByteBuffer.h"
#include <stdio.h>

/**
 * 去除PS中的扩展时间
 */
class PsCutTime
{
public:
    PsCutTime();
    virtual ~PsCutTime();

    /**
     * 去除PS包中的扩展时间
     * @param pkt       输入PS包
     * @param outPkt    输出PS包
     * @return  true 表示对PS包有改动, false 表示没有改动(outPkt无效)
     */
    bool cutTime(const PsPacket& pkt, PsPacket& outPkt);

    /**
     * 去除PS文件中的扩展时间
     * @param filepath  输入PS文件
     * @param outPath   输出PS文件
     * @return
     */
    bool cutTime(const char* filepath, const char* outPath);

protected:
    comn::ByteBuffer    m_buffer;

};

#endif /* PSCUTTIME_H_ */
