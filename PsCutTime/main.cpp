/*    file: main.cpp
 *    desc:
 * 
 * created: 2016-04-06
 *  author: chuanjiang.zh@qq.com
 * company: 
 */

#include <stdio.h>
#include <iostream>

#include "BasicType.h"
#include "SharedPtr.h"
#include "TFileUtil.h"
#include "TByteBuffer.h"
#include "BasicMacro.h"
#include "TStringUtil.h"
#include <assert.h>
#include <errno.h>

#include "PsCutTime.h"
#include "DateTime.h"
#include "Path.h"
#include <memory>


class Application 
{
public:
	Application()
	{
	}

	~Application()
	{
	}

};

int main(int argc, char** argv)
{
	std::string filename = "../../assets/20180704_141540.ps";

	if (argc > 1)
	{
		filename = argv[1];
	}

	Application app;

	PsCutTime cut;
	cut.cutTime(filename.c_str(), "cut.ps");

	return 0;
}




