#/bin/sh

PWD=`pwd`
THIS_DIR=$(cd "$(dirname "$0")"; pwd)
ROOT_DIR=$THIS_DIR/..

# build app
mkdir $ROOT_DIR/gcc
cd $ROOT_DIR/gcc

cmake ..
make -j 2

# build pack
cpack

# zip
cd $THIS_DIR


