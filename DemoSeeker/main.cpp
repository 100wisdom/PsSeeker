/*    file: main.cpp
 *    desc:
 * 
 * created: 2016-04-06
 *  author: chuanjiang.zh@qq.com
 * company: 
 */

#include <stdio.h>
#include <iostream>

#include "BasicType.h"
#include "SharedPtr.h"
#include "TFileUtil.h"
#include "TByteBuffer.h"
#include "BasicMacro.h"
#include "TStringUtil.h"
#include <assert.h>
#include <errno.h>

#include "PsSeeker.h"
#include "DateTime.h"
#include "Path.h"
#include <memory>

#include "PsFileStream.h"
#include "CPsFile.h"
#include "CPsGroupFile.h"


class Application 
{
public:
	Application()
	{
	}

	~Application()
	{
	}

};


void print(const PsFilePosition& pos)
{
	const char* type = (pos.mediaType == MTYPE_AUDIO) ? "A" : "V";
	comn::DateTime dt(pos.time);
	std::string time = dt.toString();
	printf("pos '%s', key:%d, size:%5d, time: %s, pts: %lld, offset: %lld\n", 
		type, pos.keyframe, pos.size, time.c_str(), pos.pts, pos.streamOffset);
}

bool PsPacketDumper(void* userdata, const PsFilePosition& pos, PsMPacket& pkt)
{
	printf("pkt. type: %d, size: %d, pts: %lld\n", pkt.type, pkt.size, pkt.pts);
	if (pkt.type == MTYPE_VIDEO)
	{
		comn::FileUtil::write(pkt.data, pkt.size, "dump.h264", true);
	}
	return true;
}


void testPsFileStream(const std::string& filepath)
{
	PsFileStream fs;
	if (!fs.open(filepath.c_str()))
	{
		printf("failed to open file: %s\n", filepath.c_str());
		return;
	}

	PsPacket packet = PsPacket();
	while (fs.read(packet))
	{
		printf("pkt. type: %d, size: %d, pos: %lld\n", packet.type, packet.size, packet.pos);

		if (packet.type == kPsVideoPacket)
		{
			PsPacket outPkt = PsPacket();
			if (PsFileStream::getPesData(packet, outPkt))
			{
				comn::FileUtil::write(outPkt.data, outPkt.size, "v.h264", true);
			}
		}

		int64_t pts = 0;
		if (PsFileStream::getTimeStamp(packet, pts, pts))
		{
			printf("pts: %lld\n", pts);
		}

		time_t t = 0;
		if (PsFileStream::getTime(packet, t))
		{
			comn::DateTime dt(t);
			printf("time: %s\n", dt.toString().c_str());
		}
	}
}

void testPsFile(std::string filepath)
{
	CPsFile psFile;
	if (!psFile.open(filepath.c_str()))
	{
		printf("failed to open file: %s\n", filepath.c_str());
		return;
	}

	PsFilePosition pos;

	pos = psFile.getBeginPos();
	print(pos);

	pos = psFile.getEndPos();
	print(pos);

	//psFile.readRange(psFile.getBeginPos(), PsFilePosition(), PsPacketDumper, NULL);

	PsFilePosition beginKeyPos = psFile.getBeginKeyPos();
	time_t beginTime = beginKeyPos.time;

	PsFilePosition beginPos = PsFilePosition();
	PsFilePosition endPos = PsFilePosition();
	psFile.seekRange(beginTime - 3, 8, kSeekKeyFrame, beginPos, endPos);

	psFile.readRange(beginPos, endPos, PsPacketDumper, NULL);
}

void testGroupFile()
{
	std::string filename1 = "../../assets/HX000000_北京蓝天多维_05_机械间3_20180704_141500.mp4";
	std::string filename2 = "../../assets/HX000000_北京蓝天多维_05_机械间3_20180704_143000.mp4";
	std::string filename3 = "../../assets/HX000000_北京蓝天多维_05_机械间3_20180704_144500.mp4";

	FILE_LIST files;
	memset(&files, 0, sizeof(files));
	int count = 0;
	strcpy(files.filePath[count ++], filename1.c_str());
	strcpy(files.filePath[count ++], filename2.c_str());
	strcpy(files.filePath[count ++], filename3.c_str());
	files.count = count;

	CPsGroupFile groupFile;
	groupFile.openEx(files);

	PsFilePosition pos;

	pos = groupFile.getBeginPos();
	print(pos);

	pos = groupFile.getEndPos();
	print(pos);

	//psFile.readRange(psFile.getBeginPos(), PsFilePosition(), PsPacketDumper, NULL);

	PsFilePosition beginKeyPos = groupFile.getBeginKeyPos();
	time_t beginTime = beginKeyPos.time;

	PsFilePosition beginPos = PsFilePosition();
	PsFilePosition endPos = PsFilePosition();
	groupFile.seekRange(beginTime - 3, 60 * 15 + 20, kSeekKeyFrame, beginPos, endPos);

	groupFile.readRange(beginPos, endPos, PsPacketDumper, NULL);
}


int main(int argc, char** argv)
{
	std::string filename = "../../assets/HX000000_北京蓝天多维_05_机械间3_20180704_141500.mp4";

	if (argc > 1)
	{
		filename = argv[1];
	}

	Application app;

	//testPsFileStream(filename);
	//testPsFile(filename);

	testGroupFile();

	return 0;
}




